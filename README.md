# nilcpy
needlets internal linear combination python package

## test nilc
we will test what is the proper value of harmonic domain filter and freedom selection for CMB B mode foreground removal.

First, use camb package to generate B power spectrum with lensing. Then use pysm3 package to generate foreground to implement our simple simulations.
