import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

nside = 512
n_pix = hp.nside2npix(nside)

noise = np.random.normal(0,1,(n_pix))

cl = hp.anafast(noise)
l = np.arange(len(cl))
plt.loglog(l*(l+1)*cl/(2*np.pi))
plt.show()


