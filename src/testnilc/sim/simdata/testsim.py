import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

lmax = 1000
nside = 512
npix = hp.nside2npix(nside)
cl_type = 'B'

cmb = np.load(f'../../cmbsim/cmbdata/{cl_type}/{cl_type}cmbmap.npy')
fg = np.load(f'./{cl_type}/fg.npy')
# noise = np.random.normal(0,0.1, (npix))
noise = np.load(f'./{cl_type}/noise.npy')

hp.mollview(cmb,title='cmb', norm='hist')
hp.mollview(fg[0],title='95GHz fg', norm='hist')
hp.mollview(noise[0], title='noise', norm='hist')

plt.show()

cl_fg = hp.anafast(fg[0],lmax=lmax)
cl_cmb = hp.anafast(cmb,lmax=lmax)
cl_noise = hp.anafast(noise[0],lmax=lmax)

l = np.arange(len(cl_fg))

# plt.loglog(l*(l+1)*cl_cmb/(2*np.pi), label='cmb')
# plt.loglog(l*(l+1)*cl_fg/(2*np.pi), label='95GHz fg')
# plt.loglog(l*(l+1)*cl_noise/(2*np.pi), label='noise')

plt.loglog(cl_fg[2:]/cl_cmb[2:])

plt.legend()
# plt.ylim(0,50)
# plt.xlim(50)
plt.xlabel('l')
plt.ylabel('DL')
plt.show()
