import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

lmax = 1000
nside = 512
n_pix = hp.nside2npix(nside)
n_freq = 6
cl_type = 'B'

# cl_theory = np.load(f'../cmbsim/cmbdata/cmbcl.npy')

cmb_temp = np.load(f'../cmbsim/cmbdata/{cl_type}/{cl_type}cmbmap.npy')
cmb = np.full((n_freq, n_pix), cmb_temp)
print(f'cmb.shape = {cmb.shape}')

freq_list = [95,100,143,150,217,353]
fg_list = []
for freq in freq_list:
    _fg = hp.read_map(f'../fgfilter/fgfilterdata/{cl_type}/fgfilter{freq}.fits')
    fg_list.append(_fg)

fg = np.array(fg_list)
fg = np.zeros((n_freq, n_pix))
print(f'fg.shape = {fg.shape}')

noise_sigma = 0.1
noise = np.random.normal(0,noise_sigma,(n_freq, n_pix))

sim = cmb + fg + noise
np.save(f'./simdata/{cl_type}/cmbfgnoise.npy', sim)
np.save(f'./simdata/{cl_type}/cmbfg.npy', cmb+fg)
np.save(f'./simdata/{cl_type}/cmbnoise.npy', cmb+noise)
np.save(f'./simdata/{cl_type}/fgnoise', fg+noise)
np.save(f'./simdata/{cl_type}/cmb', cmb)
np.save(f'./simdata/{cl_type}/fg', fg)
np.save(f'./simdata/{cl_type}/noise', noise)



