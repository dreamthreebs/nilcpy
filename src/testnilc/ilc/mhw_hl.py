import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

lmax = 1000
n_needlets = 20
l = np.arange(lmax + 1)
B = 1.5
hl = 0.001 * np.ones((n_needlets, lmax+1))

def get_normalized_mhw_hl():
    ''' first generate a gaussian weight hl, then use the hl2sum to normalize it, so that the hl2 at different l is zero after getting sum over every scale j '''

    for j in range(n_needlets):
        hl[j,1:] = np.sqrt(1/(2*np.pi)) * (l[1:]/B**(j+1)) * np.exp((-(1/2)*(l[1:]/B**(j+1))**2))
        plt.plot(hl[j],label=f'{j}')
    plt.xlim(0,150)
    plt.show()
    
    hl2 = hl**2
    hl2[:,0:1] = 0
    hlsum = np.sum(hl2, axis=0)
    plt.xlim(0,150)
    plt.ylim(0,1)
    plt.plot(hlsum);plt.show()
    
    for j in range(n_needlets):
        hl[j,1:] = np.sqrt(1/(2*np.pi)) * np.sqrt(1/hlsum[1:]) * (l[1:]/B**(j+1)) * np.exp((-(1/2)*(l[1:]/B**(j+1))**2))
        plt.plot(hl[j],label=f'{j}')
    
    plt.xlim(0,150)
    plt.show()
    
    hl2 = hl**2
    hl2[:,0:1] = 0
    hlsum = np.sum(hl2, axis=0)
    plt.xlim(0,150)
    plt.plot(hlsum);plt.show()
    np.save('./mhw/mexican_hl.npy',hl)

def get_cutted_mhw_hl():
    ''' sum over the first few number of filters by equation bl = np.sqrt(np.sum(bl**2), axis=0) to get more dof in the first filter '''

    hl = np.load('mexican_hl.npy')
    hl2 = hl**2
    
    hl_cut_j = 11
    hl_cut = np.zeros((n_needlets - hl_cut_j+1, lmax+1))
    
    hl_cut[0] = np.sqrt(np.sum(hl2[:hl_cut_j,:],axis=0))
    hl_cut[1:] = hl[hl_cut_j:,:]
    hl2 = hl_cut**2
    hlsum = np.sum(hl2, axis=0)
    for j in range(n_needlets-hl_cut_j+1):
        plt.plot(hl_cut[j],label=f'{j}')
    # plt.xlim(0,150)
    # plt.ylim(0,1)
    plt.show()
    
    hl2 = hl_cut**2
    hlsum = np.sum(hl2, axis=0)
    # plt.xlim(0,150)
    # plt.ylim(0,1)
    plt.plot(hlsum);plt.show()
    np.save('mhw_cut_hl.npy', hl_cut)

# plt.legend()
# plt.show()


