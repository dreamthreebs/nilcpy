import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

lmax = 1000

m_cmb = np.load('../cmbsim/cmbdata/cmb_curl.npy')
l = np.arange(lmax+1)

cl_cmb = hp.anafast(m_cmb, lmax=lmax)
cl_sd1 = np.load('./stdnilc/nilc_cl_sd16.npy')
cl_sd2 = np.load('./stdnilc/nilc_cl_sd17.npy')
# cl_sd3 = np.load('./stdnilc/nilc_cl_sd14.npy')
# cl_sd4 = np.load('./stdnilc/nilc_cl_sd.npy')
# cl_sd5 = np.load('./stdnilc/nilc_cl_sd10.npy')
# cl_sd6 = np.load('./stdnilc/nilc_cl_sd11.npy')

# plt.semilogy((1/500)*l*(l+1)*cl_cmb/(2*np.pi),label='theory')
plt.semilogy(np.abs(cl_sd1)/cl_cmb*500,label='experiment1')
plt.semilogy(np.abs(cl_sd2)/cl_cmb*500,label='experiment2')
# plt.semilogy(np.abs(cl_sd3)/cl_cmb*500,label='experiment3')
# plt.semilogy(np.abs(cl_sd4)/cl_cmb*500,label='experiment4')
# plt.loglog(l*(l+1)*np.abs(cl_sd5)/(2*np.pi),label='experiment5')
# plt.loglog(l*(l+1)*np.abs(cl_sd6)/(2*np.pi),label='experiment6')
# plt.yscale('symlog')
plt.legend()
plt.show()


