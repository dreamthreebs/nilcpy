import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

lmax = 1000
n_freq = 6
cl_type = 'B'
sim = np.load(f'../sim/simdata/{cl_type}/cmbfgnoise.npy')
fg = np.load(f'../sim/simdata/{cl_type}/fg.npy')

print(f'sim.shape = {sim.shape}')

def pixel_ilc(sim):
    C = np.zeros((n_freq, n_freq))
    C = np.cov(sim)
    A = np.zeros((n_freq+1, n_freq+1))
    A[0:-1,0:-1] = 2 * C
    A[-1,0:n_freq] = 1
    A[0:n_freq,-1] = -1
    print(f'{A = }')
    b = np.zeros(n_freq+1)
    b[-1] = 1
    print(f'{b = }')
    x = np.linalg.solve(A, b)

    weight = x[0:n_freq]
    print(f'{weight = }')

    ilc = weight @ sim
    return weight, ilc

weight, ilc_res = pixel_ilc(sim)
fgres = weight @ fg
fgres_cl = hp.anafast(fgres, lmax=lmax)


ilc_cl = hp.anafast(ilc_res, lmax=lmax)

l = np.arange(len(ilc_cl))
print(f'{len(ilc_cl) = }')
plt.loglog(l*(l+1)*ilc_cl/(2*np.pi),label='ilc res')

cl = np.load(f'../cmbsim/cmbdata/cmbcl.npy')

plt.loglog(l*(l+1)*cl[:len(ilc_cl),2]/(2*np.pi),label='pure cmb')
plt.legend()
plt.show()

np.save('./plotilcres/pixelilc_cl.npy',ilc_cl)
np.save('./plotilcres/pixelilc_fgres.npy', fgres_cl)


