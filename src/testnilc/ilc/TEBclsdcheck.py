import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
import pymaster as nmt

nside = 256

# Initialize binning scheme with bandpowers of constant width
# (4 multipoles per bin)
bin1 = nmt.NmtBin.from_nside_linear(nside, 10)

ell_eff = bin1.get_effective_ells()
print(f'{ell_eff.shape = }')

ell_arr = np.arange(768)
print(f'{ell_arr.shape = }')

cl_type = 'B'
cl_bb = np.load('../cmbsim/cmbdata/cmbcl.npy')[0:768,2]
cl_bb_binned = bin1.bin_cell(np.array([cl_bb]))

cl1 = np.load(f'./{cl_type}/nilc_clsd9.npy')[:768]
cl1_binned = bin1.bin_cell(np.array([cl1]))

cl_type = 'E'
cl_ee = np.load('../cmbsim/cmbdata/cmbcl.npy')[0:768,1]
cl_ee_binned = bin1.bin_cell(np.array([cl_ee]))


cl2 = np.load(f'./{cl_type}/nilc_clsd1.npy')[:768]
cl2_binned = bin1.bin_cell(np.array([cl2]))

cl_type = 'T'
cl_tt = np.load('../cmbsim/cmbdata/cmbcl.npy')[0:768,0]
cl_tt_binned = bin1.bin_cell(np.array([cl_tt]))

cl3 = np.load(f'./{cl_type}/nilc_clsd1.npy')[:768]
cl3_binned = bin1.bin_cell(np.array([cl3]))


# cl4 = np.load(f'./{cl_type}/nilc_clsd4.npy')[:768]
# cl4_binned = bin1.bin_cell(np.array([cl4]))
# cl5 = np.load(f'./{cl_type}/nilc_clsd5.npy')[:768]
# cl5_binned = bin1.bin_cell(np.array([cl5]))
# cl6 = np.load(f'./{cl_type}/nilc_clsd7.npy')[:768]
# cl6_binned = bin1.bin_cell(np.array([cl6]))




# Plot all to see differences
# plt.plot(ell_arr, cl/500, 'black',
         # label='Theory $C_\\ell$')
# plt.plot(ell_eff, np.abs(cl_binned[0]/500), 'g-',
         # label='theory Binned $C_\\ell$')

plt.loglog(ell_eff, np.abs(cl1_binned[0]/cl_bb_binned[0]*100),
         label='Binned $C_\\ell$')

plt.loglog(ell_eff, np.abs(cl2_binned[0]/cl_ee_binned[0]*100),
         label='Binned1  $C_\\ell$')

plt.loglog(ell_eff, np.abs(cl3_binned[0]/cl_tt_binned[0]*100),
         label='Binned2  $C_\\ell$')

# plt.loglog(ell_eff, np.abs(cl4_binned[0]/cl_cmb_binned[0]*100),
#          label='Binned3  $C_\\ell$')
# plt.loglog(ell_eff, np.abs(cl5_binned[0]/cl_cmb_binned[0]*100),
#          label='Binned4  $C_\\ell$')
# plt.loglog(ell_eff, np.abs(cl6_binned[0]/cl_cmb_binned[0]*100),
#          label='Binned5  $C_\\ell$')



# plt.loglog()
# plt.ylim(0.5,2)
plt.legend(loc='upper right', frameon=False)
plt.xlabel('ell')
plt.ylabel('cl_sd/(1/100)cl_cmb')
plt.show()


