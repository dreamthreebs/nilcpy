import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
import pymaster as nmt

nside = 256
cl_type = 'B'

# Initialize binning scheme with bandpowers of constant width
# (4 multipoles per bin)
bin1 = nmt.NmtBin.from_nside_linear(nside, 10)

ell_eff = bin1.get_effective_ells()
print(f'{ell_eff.shape = }')

ell_arr = np.arange(768)
print(f'{ell_arr.shape = }')

cl_cmb = np.load('../cmbsim/cmbdata/cmbcl.npy')[0:768,2]
cl_cmb_binned = bin1.bin_cell(np.array([cl_cmb]))

cl1 = np.load(f'./{cl_type}/nilc54_clsd1.npy')[:768]
cl1_binned = bin1.bin_cell(np.array([cl1]))

# Plot all to see differences
# plt.plot(ell_arr, cl/500, 'black',
         # label='Theory $C_\\ell$')
# plt.plot(ell_eff, np.abs(cl_binned[0]/500), 'g-',
         # label='theory Binned $C_\\ell$')

plt.loglog(ell_eff, np.abs(cl1_binned[0]/cl_cmb_binned[0]*100),
         label='Binned $C_\\ell$')

# plt.loglog(ell_eff, np.abs(cl1_tt_binned[0]/cl_binned[0]*100),
#          label='Binned1  $C_\\ell$')

# plt.loglog(ell_eff, np.abs(cl2_tt_binned[0]/cl_binned[0]*100),
#          label='Binned2  $C_\\ell$')

# plt.loglog(ell_eff, np.abs(cl3_tt_binned[0]/cl_binned[0]*500),
#          label='Binned3  $C_\\ell$')

# plt.loglog(ell_eff, np.abs(cl4_tt_binned[0]/cl_binned[0]*1000),
#          label='Binned4  $C_\\ell$')

# plt.loglog(ell_eff, np.abs(cl5_tt_binned[0]/cl_binned[0]*100),
#          label='Binned5  $C_\\ell$')

# plt.loglog(ell_eff, np.abs(cl6_tt_binned[0]/cl_binned[0]*100),
#          label='Binned6  $C_\\ell$')





# plt.plot(ell_eff, dl_tt_binned[0], 'y-',
         # label='Binned $D_\\ell 2\\pi/(\\ell(\\ell+1))$')
# plt.plot(ell_arr, cl_tt_binned_unbinned[0], 'b-',
         # label='Binned-unbinned $C_\\ell$')
# plt.loglog()
# plt.ylim(0.5,2)
plt.legend(loc='upper right', frameon=False)
plt.show()


