import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

lmax=1000
nside=512
sim = np.load('../sim/simdata/cmbfgnoise6bands.npy')
m95 = sim[0,:]

alm = hp.map2alm(m95,lmax=lmax)

filter_l = np.zeros(lmax+1)
filter_l[2:1000] = 1

alm_filtered = hp.almxfl(alm, filter_l)

m95_filtered = hp.alm2map(alm_filtered, nside=nside, lmax=lmax)

hp.mollview(m95_filtered, norm='hist')
plt.show()




