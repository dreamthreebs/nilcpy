import numpy as np

lmax = 1000
constant_value = 238888
arr = np.arange(lmax)

intervals = []
i = 0

while i < lmax:
    closest_difference = float('inf')
    best_j = i
    for j in range(i + 1, lmax + 1):
        sum_value = np.sum(2 * arr[i:j] + 1)
        difference = abs(sum_value - constant_value)
        if difference < closest_difference:
            closest_difference = difference
            best_j = j
        if sum_value > constant_value:
            break
    intervals.append((i, best_j))
    i = best_j

for interval in intervals:
    print(f"Interval: {interval}")

