import nlopt
import numpy as np

def target_function(params, grad):
    lmax1, lmax2 = params
    lmaxs = [lmax1, lmax2, 1000]
    l_range = np.arange(1001)
    desired_degrees = [23889, 160200]
    actual_degrees = []

    for i in range(len(lmaxs)):
        lmin = 0 if i == 0 else lpeak
        lpeak = 0 if i == 0 else lmaxs[i-1]
        lmax = lmaxs[i]

        hl = np.where((l_range < lmin) | (l_range > lmax), 0,
                np.where(l_range < lpeak, np.cos(((lpeak-l_range)/(lpeak-lmin+1e-15)) * np.pi/2),
                np.where(l_range >= lpeak, np.cos(((l_range-lpeak)/(lmax-lpeak+1e-15)) * np.pi/2), 1)))
        print(f'{hl.shape = }')

        # actual_degrees.append(np.sum((2 * l_range + 1) * hl**2))
        active_l_range = l_range[(l_range >= lmin) & (l_range <= lmax)]
        actual_degrees.append(np.sum((2 * active_l_range + 1) * hl[(l_range >= lmin) & (l_range <= lmax)]**2))

    # loss = np.sum((np.array(actual_degrees[:len(desired_degrees)]) - np.array(desired_degrees))**2)
    loss = np.sum(np.abs(np.array(actual_degrees[:len(desired_degrees)]) - np.array(desired_degrees)))
    return loss

opt = nlopt.opt(nlopt.GN_ISRES, 2)
opt.set_lower_bounds([100, 400])
opt.set_upper_bounds([899, 999])
opt.set_xtol_rel(1e-3)
opt.set_min_objective(target_function)
opt.set_maxeval(10000)

initial_guess = [300, 750]
result = opt.optimize(initial_guess)
opt_val = opt.last_optimum_value()

lmax1_opt, lmax2_opt = result
print(f"Optimized lmax1: {lmax1_opt}, lmax2: {lmax2_opt}")

