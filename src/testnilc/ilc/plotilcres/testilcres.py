import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

lmax = 1000
pixelilc_cl = np.load('./pixelilc_cl.npy')

l = np.arange(len(pixelilc_cl))

harmonicilc_cl = np.load('./harmonicilc_cl.npy')

nilc_cl = np.load('../stdnilc/nilc_cl52.npy')
nilc_cl1 = np.load('../stdnilc/nilc_cl53.npy')
nilc_cl2 = np.load('../stdnilc/nilc_cl54.npy')
nilc_cl3 = np.load('../stdnilc/nilc_cl9.npy')
nilc_cl4 = np.load('../stdnilc/nilc_cl10.npy')
nilc_cl5 = np.load('../stdnilc/nilc_cl11.npy')

# cl = np.load('../../cmbsim/cmbdata/cmbcl.npy')[:len(pixelilc_cl),2]
cl = hp.anafast(np.load('../../cmbsim/cmbdata/cmb_curl.npy'),lmax=lmax)

# plt.loglog(l*(l+1)*pixelilc_cl/(2*np.pi),label='pixel ilc')
# plt.loglog(l*(l+1)*harmonicilc_cl/(2*np.pi),label='harmonic ilc')
plt.loglog(l*(l+1)*nilc_cl/(2*np.pi),label='nilc')
plt.loglog(l*(l+1)*nilc_cl1/(2*np.pi),label='nilc1')
plt.loglog(l*(l+1)*nilc_cl2/(2*np.pi),label='nilc2')
# plt.loglog(l*(l+1)*nilc_cl3/(2*np.pi),label='nilc3')
# plt.loglog(l*(l+1)*nilc_cl4/(2*np.pi),label='nilc4')
# plt.loglog(l*(l+1)*nilc_cl5/(2*np.pi),label='nilc5')
plt.loglog(l*(l+1)*cl/(2*np.pi),label='pure cmb', color='b')

plt.xlim(2,800)
# plt.ylim(1e-5,1e0)
plt.legend()
plt.show()


