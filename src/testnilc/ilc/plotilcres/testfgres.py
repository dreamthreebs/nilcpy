import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

lmax = 1000
pixelilc_cl = np.load('./pixelilc_cl.npy')
pixelilc_fgres = np.load('./pixelilc_fgres.npy')

l = np.arange(len(pixelilc_cl))

harmonicilc_cl = np.load('./harmonicilc_cl.npy')
harmonicilc_fgres = np.load('./harmonicilc_fgres.npy')

# nilc_cl = np.load('./nilc_cl.npy')
nilc_fgres = np.load('../E/nilc_fgres_cl1.npy')
# nilc_fgres1 = np.load('../E/nilc_fgres_cl2.npy')
# nilc_fgres2 = np.load('../E/nilc_fgres_cl3.npy')
# nilc_fgres3 = np.load('../E/nilc_fgres_cl4.npy')
# nilc_fgres4 = np.load('../E/nilc_fgres_cl5.npy')
# nilc_fgres5 = np.load('../E/nilc_fgres_cl6.npy')
# nilc_fgres6 = np.load('../E/nilc_fgres_cl31.npy')

# cl = np.load('../../cmbsim/cmbdata/cmbcl.npy')[:len(pixelilc_cl),2]
cl = hp.anafast(np.load('../../cmbsim/cmbdata/E/Ecmbmap.npy'),lmax=lmax)

# plt.loglog(l*(l+1)*pixelilc_cl/(2*np.pi),label='pixel ilc')
# plt.loglog(l*(l+1)*pixelilc_fgres/(2*np.pi),label='pixel ilc fgres')
# plt.loglog(l*(l+1)*harmonicilc_cl/(2*np.pi),label='harmonic ilc')
# plt.loglog(l*(l+1)*harmonicilc_fgres/(2*np.pi),label='harmonic ilc fgres')
# plt.loglog(l*(l+1)*nilc_cl/(2*np.pi),label='nilc')

plt.loglog(l*(l+1)*nilc_fgres/(2*np.pi),label='nilc fg res')
# plt.loglog(l*(l+1)*nilc_fgres1/(2*np.pi),label='nilc1 fg res')
# plt.loglog(l*(l+1)*nilc_fgres2/(2*np.pi),label='nilc2 fg res')
# plt.loglog(l*(l+1)*nilc_fgres3/(2*np.pi),label='nilc3 fg res')
# plt.loglog(l*(l+1)*nilc_fgres4/(2*np.pi),label='nilc4 fg res')
# plt.loglog(l*(l+1)*nilc_fgres5/(2*np.pi),label='nilc5 fg res')
# plt.loglog(l*(l+1)*nilc_fgres6/(2*np.pi),label='nilc6 fg res')
plt.loglog(l*(l+1)*cl/(2*np.pi),label='pure cmb', color='black')

plt.title('foreground residual power spectrum')
plt.xlim(2,800)
# plt.ylim(1e-5,1e0)
plt.xlabel('l')
plt.ylabel('DL')
plt.legend()
plt.show()


