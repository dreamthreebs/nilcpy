import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

lmax = 1000
n_freq = 6
cl_type = 'B'
sim = np.load(f'../sim/simdata/{cl_type}/cmbfgnoise.npy')
fg = np.load(f'../sim/simdata/{cl_type}/fg.npy')

print(f'sim.shape = {sim.shape}')

def maps2alms(sim):
    alms_list = []
    for index in range(len(sim)):
        _alm = hp.map2alm(sim[index], lmax=lmax)
        alms_list.append(_alm)
    alms = np.array(alms_list)
    return alms

def harmonic_ilc(std_maps, wl=None):

    n_bands = len(std_maps)

    std_alms = maps2alms(std_maps)
    print(f'{std_alms.shape = }')

    if wl is None:
        R = np.empty((lmax+1, n_bands, n_bands))
        for i in range(n_bands):
            for j in range(n_bands):
                R[:, i, j] = hp.alm2cl(std_alms[i], std_alms[j])
        invR = np.linalg.inv(R[2:])
        # invR = myinv(R[2:])
        oneVec = np.ones(n_bands)
        wl_2 = (oneVec@invR).T/(oneVec@invR@oneVec + 1e-15)
        wl = np.zeros((n_bands, lmax + 1))
        wl[:,2:] = wl_2

    ilc_alms_list = []
    for i in range(n_bands):
        ilc_alms_list.append(hp.almxfl(std_alms[i], wl[i], inplace=False))

    ilc_alm = np.sum(np.array(ilc_alms_list), axis=0)
    return wl, ilc_alm

wl, ilc_alm = harmonic_ilc(sim)
_, fg_res_alm = harmonic_ilc(fg, wl=wl)
fg_res_cl = hp.alm2cl(fg_res_alm, lmax=lmax)

ilc_cl = hp.alm2cl(ilc_alm, lmax=lmax)

l = np.arange(len(ilc_cl))
print(f'{len(ilc_cl) = }')
plt.loglog(l*(l+1)*ilc_cl/(2*np.pi),label='ilc res')

cl = np.load(f'../cmbsim/cmbdata/cmbcl.npy')

plt.loglog(l*(l+1)*cl[:len(ilc_cl),2]/(2*np.pi),label='pure cmb')
plt.legend()
plt.show()

np.save('./plotilcres/harmonicilc_cl.npy',ilc_cl)
np.save('./plotilcres/harmonicilc_fgres.npy',fg_res_cl)


