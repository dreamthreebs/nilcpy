import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
np.random.seed(0)

lmax = 1000
nside = 512
n_pix = hp.nside2npix(nside)
l = np.arange(lmax+1)
n_freq = 6
freq_list = [95, 100, 143, 150,217, 353]
freq_arr = np.array(freq_list)


def generate_flat_cl(l, lmax):
    ''' generate map from flat cl with cosmic variance '''
    cl = np.zeros_like(l, dtype=float)
    cl[:lmax+1] = 1

    # plt.plot(cl); plt.show()
    return cl

def generate_step_cl(l, lmax):
    cl = np.zeros_like(l, dtype=float)

    cl[:200] = 10
    cl[200:lmax+1] = 20

    # plt.plot(cl); plt.show()
    return cl

def generate_map_from_cl(cl, nside):
    _alm = hp.synalm(cl)
    _cl = hp.alm2cl(_alm)
    alm = hp.almxfl(_alm, np.sqrt(cl/_cl))
    m = hp.alm2map(alm, nside=nside)
    # hp.mollview(m, norm='hist'); plt.show()
    return m

def power_law(amp, x, power, pivot_val):
    return amp * (x/pivot_val) ** power

def check_map(m):
    for i in range(m.shape[0]):
        hp.mollview(m[i]); plt.show()

def generate_sim(cmb_tmp, fg_tmp, amp_noise=1):
    cmb = np.full((n_freq, n_pix), cmb_tmp)

    # fg_val = power_law(0.082, freq_arr, -3.1) + power_law(0.258, freq_arr, 1.55)
    fg_val = power_law(0.082, freq_arr, -3.1, 100) + power_law(0.258, freq_arr, 1.55,100) + power_law(1, freq_arr, -5.1, 100) +  power_law(1, freq_arr, -1.3, 300) + power_law(0.2, freq_arr, 1.82, 100)
    fg = np.zeros((n_freq, n_pix))

    for i in range(n_freq):
        fg[i] = fg_tmp * fg_val[i]

    noise = np.random.normal(0,amp_noise,(n_freq, n_pix))

    sim = cmb + fg + noise
    return cmb, fg, noise, sim

def pixel_ilc(sim):
    C = np.zeros((n_freq, n_freq))
    C = np.cov(sim)
    A = np.zeros((n_freq+1, n_freq+1))
    A[0:-1,0:-1] = 2 * C
    A[-1,0:n_freq] = 1
    A[0:n_freq,-1] = -1
    print(f'{A = }')
    b = np.zeros(n_freq+1)
    b[-1] = 1
    print(f'{b = }')
    x = np.linalg.solve(A, b)

    weight = x[0:n_freq]
    print(f'{weight = }')

    ilc = weight @ sim
    return ilc
def maps2alms(_maps):
    alms_list = []
    for index in range(len(_maps)):
        _alm = hp.map2alm(_maps[index], lmax=lmax)
        alms_list.append(_alm)
    alms = np.array(alms_list)
    return alms
def myinv(a):
    if np.shape(a) == 2:
        return np.linalg.inv(a)
    res = np.zeros_like(a)
    for i in range(len(a)):
        try:
            res[i] = np.linalg.inv(a[i])
        except:
            pass
    return res

def harmonic_ilc(std_maps, wl=None):

    n_bands = len(std_maps)

    std_alms = maps2alms(std_maps)
    print(f'{std_alms.shape = }')

    if wl is None:
        R = np.empty((lmax+1, n_bands, n_bands))
        for i in range(n_bands):
            for j in range(n_bands):
                R[:, i, j] = hp.alm2cl(std_alms[i], std_alms[j])
        # invR = np.linalg.inv(R[2:])
        invR = myinv(R[2:])
        oneVec = np.ones(n_bands)
        wl_2 = (oneVec@invR).T/(oneVec@invR@oneVec + 1e-15)
        wl = np.zeros((n_bands, lmax + 1))
        wl[:,2:] = wl_2

    ilc_alms_list = []
    for i in range(n_bands):
        ilc_alms_list.append(hp.almxfl(std_alms[i], wl[i], inplace=False))

    ilc_alm = np.sum(np.array(ilc_alms_list), axis=0)
    return wl, ilc_alm


def main():
    cl_cmb = generate_flat_cl(l, lmax)
    cmb_tmp = generate_map_from_cl(cl_cmb, nside)
    cl_fg = generate_step_cl(l, lmax)
    fg_tmp = generate_map_from_cl(cl_fg, nside)

    cmb, fg, noise, sim = generate_sim(cmb_tmp, fg_tmp, amp_noise=30)

    ilc_map_res = pixel_ilc(sim)
    pixilc_res = hp.map2alm(ilc_map_res)
    wl, hilc_res = harmonic_ilc(sim)

    return locals()

def check_res(var):
    pixilc_res = var['pixilc_res']
    hilc_res = var['hilc_res']
    cmb_tmp = var['cmb_tmp']
    cl_pixilc_res = hp.alm2cl(pixilc_res)
    cl_hilc_res = hp.alm2cl(hilc_res)
    cl_cmb = hp.anafast(cmb_tmp)
    plt.plot(cl_cmb[2:lmax],label='pure cmb')
    plt.plot(cl_hilc_res[2:lmax],label='harmonic ilc')
    plt.plot(cl_pixilc_res[2:lmax],label='pixel ilc')
    plt.legend()
    plt.show()

if __name__=='__main__':
    var = main()
    check_res(var)

    # fg_val = power_law(0.082, freq_arr, -3.1, 100) + power_law(0.258, freq_arr, 1.55,100) + power_law(1, freq_arr, -5.1, 100) +  power_law(1, freq_arr, -1.3, 300) + power_law(0.2, freq_arr, 1.82, 100)
    # plt.plot(freq_arr, fg_val); plt.show()


