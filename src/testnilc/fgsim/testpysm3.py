import pysm3
import pysm3.units as u
import healpy as hp
import numpy as np

import warnings
warnings.filterwarnings("ignore")
sky = pysm3.Sky(nside=512, preset_strings=['d1','s1'])

sky.components
map_100GHz = sky.get_emission(100 * u.GHz)
map_100GHz[0, :3]
map_100GHz = map_100GHz.to(u.uK_CMB, equivalencies=u.cmb_equivalencies(100*u.GHz))
hp.mollview(map_100GHz[0], min=0, max=1e2, title="I map", unit=map_100GHz.unit)
hp.mollview(np.sqrt(map_100GHz[1]**2 + map_100GHz[2]**2), title="P map", min=0, max=1e1, unit=map_100GHz.unit)




