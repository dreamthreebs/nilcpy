import pysm3
import pysm3.units as u
import healpy as hp
import numpy as np
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings("ignore")
sky = pysm3.Sky(nside=512, preset_strings=['d1','s1'])

freq_list = [95,100,143,150,217,353]

for freq in freq_list:

    _fg = sky.get_emission(freq * u.GHz)
    _fg = _fg.to(u.uK_CMB, equivalencies=u.cmb_equivalencies(freq*u.GHz))

    # hp.mollview(_fg[0], title=f"I map at {freq}GHz", unit=_fg.unit)
    # hp.mollview(np.sqrt(_fg[1]**2 + _fg[2]**2), title=f"P map at {freq}GHz", unit=_fg.unit)
    # plt.show()

    hp.write_map(f'./fgdata/fg{freq}.fits', _fg, overwrite=True)


