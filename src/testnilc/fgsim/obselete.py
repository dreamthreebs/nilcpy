import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
from synfast_sim import generate_flat_cl, generate_step_cl

lmax = 20
nside = 8
l = np.arange(lmax+1)

cl_cmb = generate_flat_cl(l, lmax)
cl_fg = generate_step_cl(l, lmax)

# alm = np.zeros(hp.Alm.getsize(lmax))
# for l in range(lmax+1):
#     for m in range(l+1):
#         alm[hp.Alm.getidx(lmax, l, m)] = np.random.normal(scale=np.sqrt(cl_cmb[l]))
#         print(f'{hp.Alm.getidx(lmax, l, m) = }')

alm = np.zeros(hp.Alm.getsize(lmax), dtype=np.complex128)
for l in range(lmax + 1):
    for m in range(l + 1):
        real_part = np.random.normal(scale=np.sqrt(cl_cmb[l] / 2))
        imag_part = np.random.normal(scale=np.sqrt(cl_cmb[l] / 2))
        alm[hp.Alm.getidx(lmax, l, m)] = complex(real_part, imag_part)

def sim_cmb(cl, nside, n_sim):
    ''' obselete '''
    cmb_cl = 0
    for i in range(n_sim):
        cmb_sim = hp.synfast(cl, nside=nside)
        cmb_cl = cmb_cl + hp.anafast(cmb_sim)
    cmb_cl = cmb_cl / n_sim
    return cmb_cl


