import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

lmax = 1000
nside = 512
freq_list = [95,100,143,150,217,353]
l = np.arange(lmax+1)

filter_lmin = 80
filter_lmax = 200

def filter_l(alm, lmin, lmax):
    lmax_alm = hp.Alm.getlmax(len(alm))
    fl = np.zeros(lmax_alm + 1)
    # fl[lmin:lmax + 1] = 1
    fl[:filter_lmin] = 1
    fl[filter_lmin:filter_lmax] = 1
    fl[filter_lmax:lmax_alm+1] = 1

    return hp.almxfl(alm, fl)

for index, cl_type in enumerate('TEB'):
    for freq in freq_list:
        _fg = hp.read_map(f'../fgsim/fgdata/fg{freq}.fits',field=(0,1,2))
        _fgalm = hp.map2alm(_fg, lmax=lmax)
        _fg_filt_alm = filter_l(_fgalm[index], filter_lmin, filter_lmax)
        _fg_filt_m = hp.alm2map(_fg_filt_alm, nside=nside, lmax=lmax)
        hp.write_map(f'./fgfilterdata/{cl_type}/fgfilter{freq}.fits', _fg_filt_m, overwrite=True)

