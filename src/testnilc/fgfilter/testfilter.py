import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

lmax = 1000
freq_list = [95,100,143,150,217,353]
l = np.arange(lmax+1)

# for freq in freq_list:
#     hp.read_map(f'')

fg95 = hp.read_map('../fgsim/fgdata/fg95.fits',field=(0,1,2))

fgalm95 = hp.map2alm(fg95, lmax=lmax)

def filter_l(alm, lmin, lmax):
    lmax_alm = hp.Alm.getlmax(len(alm))
    fl = np.zeros(lmax_alm + 1)
    # fl[lmin:lmax + 1] = 1
    fl[:lmin] = 1/5
    fl[lmin:lmax_alm+1] = 1

    return hp.almxfl(alm, fl)

# Set your desired l range
lmin = 500
lmax = 700

# Apply the filter
fgfiltered95 = filter_l(fgalm95[2], lmin, lmax)

