import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

m = hp.read_map('./fgfilter95.fits')
hp.mollview(m, norm='hist')
plt.show()

cl = hp.anafast(m)
l = np.arange(len(cl))
plt.loglog(cl)
plt.show()
