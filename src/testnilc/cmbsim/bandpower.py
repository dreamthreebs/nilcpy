import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
import pymaster as nmt

nside = 256


# Initialize binning scheme with bandpowers of constant width
# (4 multipoles per bin)
bin1 = nmt.NmtBin.from_nside_linear(nside, 10)

bin3 = nmt.NmtBin.from_nside_linear(nside, 10, is_Dell=True)

ell_eff = bin1.get_effective_ells()
print(f'{ell_eff.shape = }')

# Bandpower info:
print("Bandpower info:")
print(" %d bandpowers" % (bin1.get_n_bands()))
print("The columns in the following table are:")
print(" [1]-band index, [2]-list of multipoles, "
      "[3]-list of weights, [4]=effective multipole")
for i in range(bin1.get_n_bands()):
    print(i, bin1.get_ell_list(i), bin1.get_weight_list(i), ell_eff[i])
print("")

# Binning a power spectrum
# Read the TT power spectrum

# data = np.loadtxt("cls.txt", unpack=True)
# ell_arr = data[0]
# cl_tt = data[1]

# cl_tt = np.load('./cmbdata/cmbcl.npy')[0:1536,2]
cl_tt = np.load('../ilc/stdnilc/nilc_cl_sd51.npy')[0:768]
print(f'{cl_tt.shape = }')

ell_arr = np.arange(768)
print(f'{ell_arr.shape = }')
s = np.array([cl_tt])
print(f'{s.shape = }')

# Bin the power spectrum into bandpowers
cl_tt_binned = bin1.bin_cell(np.array([cl_tt]))
# For bin3 we need to correct for the ell prefactor
ellfac = ell_eff * (ell_eff + 1.)/2/np.pi
dl_tt_binned = bin3.bin_cell(np.array([cl_tt]))/ellfac
# Unbin bandpowers
cl_tt_binned_unbinned = bin1.unbin_cell(cl_tt_binned)

cl = np.load('./cmbdata/cmbcl.npy')[0:768,1]
cl_binned = bin1.bin_cell(np.array([cl]))

cl1 = np.load('../ilc/stdnilc/nilc_cl_sd49.npy')[:768]
cl1_tt_binned = bin1.bin_cell(np.array([cl1]))

cl2 = np.load('../ilc/stdnilc/nilc_cl_sd52.npy')[:768]
cl2_tt_binned = bin1.bin_cell(np.array([cl2]))

cl3 = np.load('../ilc/stdnilc/nilc_cl_sd53.npy')[:768]
cl3_tt_binned = bin1.bin_cell(np.array([cl3]))

cl4 = np.load('../ilc/stdnilc/nilc_cl_sd54.npy')[:768]
cl4_tt_binned = bin1.bin_cell(np.array([cl4]))

cl5 = np.load('../ilc/stdnilc/nilc_cl_sd50.npy')[:768]
cl5_tt_binned = bin1.bin_cell(np.array([cl5]))

cl6 = np.load('../ilc/stdnilc/nilc_cl_sd25.npy')[:768]
cl6_tt_binned = bin1.bin_cell(np.array([cl6]))





# Plot all to see differences
# plt.plot(ell_arr, cl/500, 'black',
         # label='Theory $C_\\ell$')
# plt.plot(ell_arr, cl_tt, 'r-',
         # label='Original $C_\\ell$')
# plt.plot(ell_eff, np.abs(cl_binned[0]/500), 'g-',
         # label='theory Binned $C_\\ell$')

plt.loglog(ell_eff, np.abs(cl_tt_binned[0]/cl_binned[0]*100),
         label='Binned $C_\\ell$')
plt.loglog(ell_eff, np.abs(cl1_tt_binned[0]/cl_binned[0]*100),
         label='Binned1  $C_\\ell$')

plt.loglog(ell_eff, np.abs(cl2_tt_binned[0]/cl_binned[0]*100),
         label='Binned2  $C_\\ell$')

plt.loglog(ell_eff, np.abs(cl3_tt_binned[0]/cl_binned[0]*500),
         label='Binned3  $C_\\ell$')

plt.loglog(ell_eff, np.abs(cl4_tt_binned[0]/cl_binned[0]*1000),
         label='Binned4  $C_\\ell$')

# plt.loglog(ell_eff, np.abs(cl5_tt_binned[0]/cl_binned[0]*100),
#          label='Binned5  $C_\\ell$')

# plt.loglog(ell_eff, np.abs(cl6_tt_binned[0]/cl_binned[0]*100),
#          label='Binned6  $C_\\ell$')





# plt.plot(ell_eff, dl_tt_binned[0], 'y-',
         # label='Binned $D_\\ell 2\\pi/(\\ell(\\ell+1))$')
# plt.plot(ell_arr, cl_tt_binned_unbinned[0], 'b-',
         # label='Binned-unbinned $C_\\ell$')
# plt.loglog()
# plt.ylim(0.5,2)
plt.legend(loc='upper right', frameon=False)
plt.show()


