import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

cmbcl = np.load('./cmbdata/cmbcl.npy') # (n_ell, n_cl) TT, EE, BB, PP
l = np.arange(cmbcl.shape[0])
print(f'{cmbcl.shape}')

lmax = 1000
nside = 512

for index, cmb_type in enumerate('TEB'):
    cl = cmbcl[:,index]
    m = hp.synfast(cl, nside, lmax)
    hp.mollview(m, norm='hist')
    plt.show()
    np.save(f'./cmbdata/{cmb_type}/{cmb_type}cmbmap.npy', m)
