import numpy as np
import matplotlib.pyplot as plt
import healpy as hp

b = np.load('./B/Bcmbmap.npy')
cl = hp.anafast(b)
l = np.arange(len(cl))
plt.loglog(l*(l+1)*cl/(2*np.pi))
plt.show()

